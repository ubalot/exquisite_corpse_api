# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

users = User.create([
    {
        username: "admin",
        email: "admin@example.org",
        password: "96f7aaa4842a2b04221a446b9d69911cf868dc74e002585a98305204c11ff431",
        salt: "z4mLGmnM36s="
    },
    {
        username: "pablo",
        email: "pablo@example.org",
        password: "96f7aaa4842a2b04221a446b9d69911cf868dc74e002585a98305204c11ff431",
        salt: "z4mLGmnM36s="
    },
    {
        username: "elen",
        email: "elen@example.org",
        password: "96f7aaa4842a2b04221a446b9d69911cf868dc74e002585a98305204c11ff431",
        salt: "z4mLGmnM36s="
    },
    {
        username: "mark",
        email: "mark@example.org",
        password: "96f7aaa4842a2b04221a446b9d69911cf868dc74e002585a98305204c11ff431",
        salt: "z4mLGmnM36s="
    },
    {
        username: "peter",
        email: "peter@example.org",
        password: "96f7aaa4842a2b04221a446b9d69911cf868dc74e002585a98305204c11ff431",
        salt: "z4mLGmnM36s="
    }
])

pablo_stories = Story.create([
    {
        title: "Pablito's Title",
        user: users[1],
        phrases_limit: 10,
        words_per_phrase_limit: 1
    }
])

pablo_story_words = Word.create([
    {
        text: "Prima",
        index: 1,
        user: users[1],
        story: pablo_stories[0]
    },
    {
        text: "stupidaggine!",
        index: 2,
        user: users[2],
        story: pablo_stories[0]
    }
])