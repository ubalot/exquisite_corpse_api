class User < ApplicationRecord
  has_many :stories
  has_many :words

  def as_json(*args)
    transformed = {
      :id => self.id,
      :username => self.username,
      :email => self.email,
    }
    transformed[:password] = self.password unless Rails.env.production?
    transformed
  end
end
