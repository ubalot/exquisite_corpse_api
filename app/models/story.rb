class Story < ApplicationRecord
  belongs_to :user
  has_many :words

  def as_json(*args)
    {
      :id => self.id,
      :title => self.title,
      :actual_phrases_count => self.words.length,
      :phrases_limit => self.phrases_limit,
      :words_per_phrase_limit => self.words_per_phrase_limit,
      :author => self.user,
      :words => self.words,
      :created_at => self.created_at,
      :updated_at => self.updated_at
    }
  end
end
