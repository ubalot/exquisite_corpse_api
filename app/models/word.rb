class Word < ApplicationRecord
  belongs_to :story
  belongs_to :user #, :foreign_key => :story

  def as_json(*args)
    {
      :text => self.text,
      :index => self.index,
      :created_at => self.created_at,
      :updated_at => self.updated_at,
      :story_id => self.story.id,
      :author_id => self.user.id
      # :author => User.find(self.user_id),
      # :story => Story.find(self.story_id)
      # :author => self.user,
      # :story => self.story
    }
  end
end
