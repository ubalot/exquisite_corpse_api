class Api::V1::WordsController < ApplicationController
  before_action :set_word, only: [:show, :edit, :update, :destroy]

  # GET /words
  # GET /words.json
  def index
    @words = Word.all
    render json: @words
  end

  # GET /words/1
  # GET /words/1.json
  def show
  end

  # GET /words/new
  def new
    @word = Word.new
  end

  # GET /words/1/edit
  def edit
  end

  # POST /words
  # POST /words.json
  def create
    user_id = word_params[:user_id]
    begin
      @user = User.find(user_id)
    rescue
      return render json: {
        :success => 0,
        :message => "User with id '#{user_id}' doesn't exists."
      }
    end

    story_id = word_params[:story_id]
    begin
      @story = Story.find(story_id)
    rescue
      return render json: {
        :success => 0,
        :message => "Story with id '#{story_id}' doesn't exists."
      }
    end

    return render json: {
      :success => 0,
      :message => "You can't write two phrases in sequence."
    } if  not @story.words.empty? and @story.words.last.user.id == user_id

    return render json: {
      :success => 0,
      :message => "Phrase contains too much word. Max: #{words_per_phrase_limit}"
    } if word_params[:text].split.length > @story.words_per_phrase_limit

    return render json: {
      :success => 0,
      :message => "Story reached its maximum number of words."
    } unless @story.words.length < @story.phrases_limit

    params = word_params.clone
    params[:user] = @user
    params[:story] = @story
    params[:index] = @story.words.length + 1
    @word = Word.new(params)

    if @word.save
      render json: {
        :success => 1,
        :data => {
          :word => @word
        }
      }
    else
      render json: {
        :success => 0,
        :message => "Word creation FAILED."
      }
    end
  end

  # PATCH/PUT /words/1
  # PATCH/PUT /words/1.json
  def update
    respond_to do |format|
      if @word.update(word_params)
        format.html { redirect_to @word, notice: 'Word was successfully updated.' }
        format.json { render :show, status: :ok, location: @word }
      else
        format.html { render :edit }
        format.json { render json: @word.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /words/1
  # DELETE /words/1.json
  def destroy
    @word.destroy
    render json: {
      :success => 1,
      :data    => {
        :word => @word
      }
    }
    # respond_to do |format|
    #   format.html { redirect_to words_url, notice: 'Word was successfully destroyed.' }
    #   format.json { head :no_content }
    # end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_word
      @word = Word.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def word_params
      # params.require(:word).permit(:text, :index, :user_id, :story_id)
      params.permit(:text, :user_id, :story_id)
    end
end
