require 'securerandom'

def encrypt_password(salt, password)
  Digest::SHA2.hexdigest(salt + password)
end

class Api::V1::UsersController < ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :destroy]

  # GET /users
  # GET /users.json
  def index
    @users = User.all
    render json: {
      :success => 1,
      :data => {
        :users => @users
      }
    }
  end

  # GET /users/1
  # GET /users/1.json
  def show
  end

  # GET /users/new
  def new
    @user = User.new
  end

  # GET /users/1/edit
  def edit
  end

  # POST /users
  # POST /users.json
  def create
    return render json: {
      :success => 0,
      :message => "Password cant' be empty."
    } unless user_params[:password].length > 0

    if User.find_by(username: user_params[:username])
      render json: {
        :success => 0,
        :data    => {
          :message => "USERNAME already taken.",
        }
      }
    elsif User.find_by(email: user_params[:email])
      render json: {
        :success => 0,
        :data    => {
          :message => "EMAIL already taken.",
        }
      }
    else
      salt = SecureRandom.base64(8)
      hashed_password = encrypt_password(salt, user_params[:password])

      tmp_params = user_params.clone
      tmp_params[:password] = hashed_password
      tmp_params[:salt] = salt

      @user = User.new(tmp_params)
      if @user.save
        render json: {
          :success => 1,
          :data    => {
            :message => "User saved succesfully.",
            :user    => @user
          }
        }
      else
        render json: {
          :success => 0,
          :data    => {
            :message => "User \"#{@user.username}\" could not be saved.",
            :user    => @user
          }
        }
      end
    end
  end

  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
    # respond_to do |format|
    #   if @user.update(user_params)
    #     format.html { redirect_to @user, notice: 'User was successfully updated.' }
    #     format.json { render :show, status: :ok, location: @user }
    #   else
    #     format.html { render :edit }
    #     format.json { render json: @user.errors, status: :unprocessable_entity }
    #   end
    # end
    if @user.update(user_params)
      render json: {
        :success => 1,
        :data    => {
          :message => "User updated succesfully.",
          :user    => User.find(@user.id)
        }
      }
    else
      render json: {
        :success => 0,
        :data    => {
          :message => "User update failed.",
          :user    => @user
        }
      }
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user.destroy
    render json: {
      :success => 1,
      :data    => {
        :user => @user
      }
    }
  end

  # POST /users/login
  def login
     @user = User.find_by(email: user_params[:email]) || User.find_by(username: user_params[:username])
    return render json: {
      :success => 0,
      :message => user_params[:email].empty? ? "Username not found." : "Email not found."
    } unless @user

    hashed_password = encrypt_password(@user.salt, user_params[:password])
    if @user.password == hashed_password
      render json: {
        :success => 1,
        :data => {
          :user => @user
        }
      }
    else
      render json: {
        :success => 0,
        :message => "Password doesn't match."
      }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      # params.require(:user).permit(:username, :email, :password)
      params.permit(:username, :email, :password)
    end
end
