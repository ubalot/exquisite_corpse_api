class Api::V1::StoriesController < ApplicationController
  before_action :set_story, only: [:show, :edit, :update, :destroy]

  # GET /stories
  # GET /stories.json
  def index
    @stories = Story.all
    render json: @stories
    # transformed = @stories.map { |story|
    #   story[:user] = User.find(story[:user_id])
    # }
    # render json: transformed
  end

  # GET /stories/1
  # GET /stories/1.json
  def show
    render json: @story
  end

  # GET /stories/new
  def new
    @story = Story.new
  end

  # GET /stories/1/edit
  def edit
  end

  # POST /stories
  # POST /stories.json
  def create
    user_id = story_params[:user_id]
    begin
      user = User.find(user_id)
    rescue
      return render json: {
        :success => 0,
        :message => user_id.is_a? Integer ? "User with id #{user_id} not found." : "You must login before creating a story"
      }
    end

    return render json: {
      :success => 0,
      :message => "Stories can't contains less than 6 words."
    } if story_params[:phrases_limit].to_i < 6

    return render json: {
      :success => 0,
      :message => "Stories must allow at least 1 word per phrase."
    } if story_params[:words_per_phrase_limit].to_i < 1

    @story = Story.new(story_params)

    if @story.save
      render json: {
        :success => 1,
        :data    => {
          :story => @story
        }
      }
    else
      render json: {
        :success => 0,
        :message => "Story creation FAILED."
      }
    end
  end

  # PATCH/PUT /stories/1
  # PATCH/PUT /stories/1.json
  def update
    respond_to do |format|
      if @story.update(story_params)
        format.html { redirect_to @story, notice: 'Story was successfully updated.' }
        format.json { render :show, status: :ok, location: @story }
      else
        format.html { render :edit }
        format.json { render json: @story.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /stories/1
  # DELETE /stories/1.json
  def destroy
    @story.destroy
    render json: {
      :success => 1,
      :data    => {
        :story => @story
      }
    }
    # respond_to do |format|
    #   format.html { redirect_to stories_url, notice: 'Story was successfully destroyed.' }
    #   format.json { head :no_content }
    # end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_story
      @story = Story.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def story_params
      # params.require(:story).permit(:title, :words_count, :user_id)
      params.permit(:title, :phrases_limit, :words_per_phrase_limit, :user_id)
    end
end
