json.extract! story, :id, :title, :words_count, :created_at, :updated_at
json.url story_url(story, format: :json)
