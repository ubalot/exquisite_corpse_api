# Exquisite Corpse API

A running app is already deployed to Heroku: [Exquisite Corpse API](https://exquisite-corpse-api.herokuapp.com/)


## Running Locally

Make sure you have Ruby installed.  Also, install the [Heroku CLI](https://devcenter.heroku.com/articles/heroku-cli) (formerly known as the Heroku Toolbelt).

Clone code:
```sh
$ git clone https://gitlab.com/ubalot/exquisite_corpse_api.git
$ cd exquisite_corpse_api
```

Use `schema.rb` to initialize the database and populate it.
```sh
$ bundle exec rake db:schema:load db:create db:seed
```
<!-- $ bundle exec rake db:create db:migrate -->

To start the app run
```sh
$ heroku local
```

Your app should now be running on [localhost:5000](http://localhost:5000/).

## Deploying to Heroku

```sh
$ git push heroku master
$ heroku run rake db:migrate
$ heroku open
```

## Docker

The app can be run and tested using the [Heroku Docker CLI plugin](https://devcenter.heroku.com/articles/local-development-with-docker-compose).

Make sure the plugin is installed:

    heroku plugins:install heroku-docker

Configure Docker and Docker Compose:

    heroku docker:init

And run the app locally:

    docker-compose up web

The app will now be available on the Docker daemon IP on port 8080.

To work with the local database and do migrations, you can open a shell:

    docker-compose run shell
    bundle exec rake db:migrate

You can also use Docker to release to Heroku:

    heroku create
    heroku docker:release
    heroku open

## Documentation

For more information about using Ruby on Heroku, see these Dev Center articles:

- [Ruby on Heroku](https://devcenter.heroku.com/categories/ruby)


## Contributing
Destroy and create a new db instance with schema.
```
bundle exec rake db:drop && bundle exec rake db:create && bundle exec rake db:schema:load && bundle exec rake db:seed && heroku local
```